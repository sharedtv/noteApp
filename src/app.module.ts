import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { NotesModule } from './notes/note.module';

@Module({
  imports: [
    NotesModule,
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: '127.0.0.1',
      port: 3306,
      username: 'root',
      password: '',
      database: 'noteapp',
      synchronize: true,
      autoLoadEntities: true,
      // entities: [Note],
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
