import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { NotesService } from './notes.service';
import { CreateNoteDto } from './dto/create-note.dto';
import { UpdateNoteDto } from './dto/update-note.dto';
import {
  ApiConflictResponse,
  ApiCreatedResponse,
  ApiNoContentResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { NoteDto } from './dto/note.dto';

@Controller('notes')
@ApiTags('notes')
export class NotesController {
  constructor(private readonly notesService: NotesService) {}

  @Post()
  @ApiCreatedResponse({
    description: 'The record has been successfully created.',
    type: NoteDto,
  })
  @ApiConflictResponse({
    description: 'Existing note title',
  })
  async create(@Body() createNoteDto: CreateNoteDto): Promise<NoteDto> {
    const note = await this.notesService.create(createNoteDto);
    return new NoteDto(note);
  }

  @Get()
  @ApiQuery({
    name: 'title',
    required: false,
  })
  @ApiQuery({
    name: 'content',
    required: false,
  })
  @ApiOkResponse({
    description: 'List of notes',
    type: [NoteDto],
  })
  async findAll(
    @Query('title') title?: string,
    @Query('content') content?: string,
  ): Promise<NoteDto[]> {
    console.log({ title, content });
    const notes = await this.notesService.findAll({ title, content });
    return notes.map((note) => new NoteDto(note));
  }

  @Get(':id')
  @ApiOkResponse({
    description: 'The note',
    type: NoteDto,
  })
  @ApiNotFoundResponse({
    description: 'Note not found',
  })
  async findOne(@Param('id') id: string): Promise<NoteDto> {
    const note = await this.notesService.findOne(+id);
    return new NoteDto(note);
  }

  @Patch(':id')
  @ApiNoContentResponse({
    description: 'Successful update',
  })
  update(
    @Param('id') id: string,
    @Body() updateNoteDto: UpdateNoteDto,
  ): Promise<void> {
    return this.notesService.update(+id, updateNoteDto);
  }

  @Delete(':id')
  @ApiNoContentResponse({
    description: 'Successful deletion',
  })
  remove(@Param('id') id: string): Promise<void> {
    return this.notesService.remove(+id);
  }
}
