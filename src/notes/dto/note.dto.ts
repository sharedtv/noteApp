import { ApiProperty } from '@nestjs/swagger';
import { Note } from '../entities/note.entity';

type NotesProperties = Required<NoteDto>;

export enum State {
  ACTIVE = 'ACTIVE',
  DEACTIVATED = 'DEACTIVATED',
}

export class NoteDto {
  @ApiProperty()
  id: number;
  @ApiProperty({ example: 'Nouvelle note' })
  title: string;
  @ApiProperty({ example: 'Une note vide' })
  content: string;
  @ApiProperty()
  state: string;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  updatedAt: Date;

  constructor(value: Note) {
    this.id = value.id ?? 0;
    this.title = value.title ?? 'Nouvelle note';
    this.content = value.content ?? '';
    this.state = value.state ?? State.ACTIVE;
    this.createdAt = value.createdAt ?? new Date();
    this.updatedAt = value.updatedAt ?? new Date();
  }
}
