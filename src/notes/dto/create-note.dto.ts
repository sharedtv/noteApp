import { ApiProperty } from '@nestjs/swagger';

export class CreateNoteDto {
  @ApiProperty({ example: 'Nouvelle note' })
  title: string;
  @ApiProperty({ example: 'Une note vide' })
  content: string;

  constructor(title: string, content: string) {
    this.title = title;
    this.content = content;
  }
}
