export interface ListFilter {
  title?: string;
  content?: string;
}
